﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CarLog.Domain.Entities
{
    public class ChargeLog : LogBase
    {
        public decimal StartProcent { get; set; }
        public decimal EndProcent { get; set; }
        public bool IsPublicCharger { get; set; }
        public bool IsActive { get; set; }
        public decimal ChargerPower { get; set; }
        public ChargeProvider ChargerProvider { get; set; }
        public decimal StartIndex { get; set; }
        public decimal EndIndex { get; set; }
        public decimal UnitCost { get; set; }
        public decimal ChargingCost { get; set; }
        public decimal ChargedUnits { get; set; }
        public int ChargingMinutes { get; set; }
        public DateTime TimeStamp { get; set; }
        public int SocketType { get; set; }
    }
}
