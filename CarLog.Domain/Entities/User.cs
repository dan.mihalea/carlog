﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CarLog.Domain.Entities
{
    public class User : LogBase
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public bool IsActive { get; set; }

        public List<UserRole> Roles { get; set; }
        public List<UserCar> UserCars { get; set; }
    }
}
