﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CarLog.Domain.Entities
{
    public class CarManufacturer : LogBase
    {
        public string Name { get; set; }
        public List<UserCar> UserCars { get; set; }
    }
}
