﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CarLog.Domain.Entities
{
    public class CarModel : LogBase
    {
        public string Name { get; set; }
        public decimal GrossBatteryValue { get; set; }
        public decimal NetBatteryValue { get; set; }
        public List<UserCar> UserCars { get; set; }
    }
}
