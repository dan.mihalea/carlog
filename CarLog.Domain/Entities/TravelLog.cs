﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace CarLog.Domain.Entities
{
    public class TravelLog : LogBase
    {
        public decimal Distance { get; set; }
        public decimal Consumption { get; set; }        
        public int ElapsedMinutes { get; set; }
        public decimal Temperature { get; set; }
        public DateTime Date { get; set; }
        public bool IsLeg { get; set; }
        [NotMapped]
        public decimal AverageSpeed => Distance * 60 / ElapsedMinutes;
        [NotMapped]
        public decimal EnergyConsumed => Distance * Consumption / 100;
    }
}
