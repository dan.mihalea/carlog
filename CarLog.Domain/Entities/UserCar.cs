﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CarLog.Domain.Entities
{
    public class UserCar : LogBase
    {
        public string CarAlias { get; set; }

        public int UserId { get; set; }
        public int CarManufacturerId { get; set; }
        public int CarModelId { get; set; }

        public User User { get; set; }
        public CarManufacturer CarManufacturer { get; set; }
        public CarModel CarModel { get; set; }

    }
}
