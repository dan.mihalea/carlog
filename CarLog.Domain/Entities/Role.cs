﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CarLog.Domain.Entities
{
    public class Role : LogBase
    {
        public string RoleName { get; set; }
        public List<UserRole> Users { get; set; }
    }
}
