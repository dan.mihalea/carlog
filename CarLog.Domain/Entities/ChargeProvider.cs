﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CarLog.Domain.Entities
{
    public class ChargeProvider : LogBase
    {
        public string ProviderName { get; set; }
    }
}
