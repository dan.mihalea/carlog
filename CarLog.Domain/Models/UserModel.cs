﻿using CarLog.Domain.Entities;
using CarLog.Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace CarLog.Domain.Models
{
    public class UserModel
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public bool IsActive { get; set; }
        public string[] Roles { get; set; }
    }
}
