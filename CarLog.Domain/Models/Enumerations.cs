﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CarLog.Domain.Models
{
    public enum ActionType
    {
        ChargeStart,
        ChargeEnd
    }
    public enum ChargerType
    {
        AC,
        DC
    }    
}
