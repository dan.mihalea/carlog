﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CarLog.Domain.Models
{
    public class ErrorModel
    {
        public string FieldName { get; set; }
        public string ErrorMessage { get; set; }
    }
}
