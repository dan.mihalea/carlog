﻿using System;

namespace CarLog.Domain.Requests
{
    public class TravelRequest
    {
        public DateTime TravelDate { get; set; }
        public decimal TravelDistance { get; set; }
        public decimal TravelConsumption { get; set; }
        public int TravelMinutes { get; set; }
        public decimal TravelTemperature { get; set; }
        public bool IsLegTravel { get; set; }
    }
}
