﻿using CarLog.Domain.Entities;
using CarLog.Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace CarLog.Domain.Requests
{
    public class ChargeRequest
    {
        public DateTime TimeStamp { get; set; }
        public int ChargeId { get; set; }
        public int StationProvider { get; set; }
        public decimal StationPower { get; set; }
        public ActionType StationAction { get; set; }
        public ChargerType ChargerType { get; set; }
        public bool IsPublicCharger { get; set; }
        public decimal? UnitCost { get; set; }
        public decimal? ChargedUnits { get; set; }
        public decimal CarPercentageStart { get; set; }
        public decimal CarPercentageEnd { get; set; }
        public decimal? ChargeIndex { get; set; }
        public int ChargedMinutes { get; set; }
    }
}