﻿using System;

namespace CarLog.Domain.Responses
{
    public class TravelResponse
    {
        public int Id { get; set; }
        public DateTime TravelDate { get; set; }
        public decimal TravelDistance { get; set; }
        public decimal TravelConsumption { get; set; }
        public int TravelMinutes { get; set; }
        public decimal TravelTemperature { get; set; }
        public decimal TravelAverageSpeed { get; set; }
        public decimal TravelEnergyConsumed { get; set; }
        public bool IsLegTravel { get; set; }
    }
}
