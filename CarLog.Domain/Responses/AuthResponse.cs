﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CarLog.Domain.Responses
{
    public class AuthResponse
    {
        public bool IsAuthSuccessful { get; set; }
        public string ErrorMessage { get; set; }
        public string Token { get; set; }
    }
}
