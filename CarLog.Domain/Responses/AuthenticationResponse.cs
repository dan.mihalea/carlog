﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CarLog.Domain.Responses
{
    public class AuthenticationResponse
    {
        public string Token { get; set; }
    }
}
