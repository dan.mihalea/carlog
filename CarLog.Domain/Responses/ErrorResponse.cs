﻿using CarLog.Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace CarLog.Domain.Responses
{
    public class ErrorResponse
    {
        public List<ErrorModel> Errors { get; set; } = new List<ErrorModel>();
    }
}
