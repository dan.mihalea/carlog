﻿using CarLog.Domain.Entities;
using CarLog.EF.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarLog.Business.Services.DataListService
{
    public class DataListService : IDataListService, IServiceMarker
    {
        private readonly IDataService<ChargeProvider> _chargeProviderDataService;
        public DataListService(IDataService<ChargeProvider> chargeProviderDataService)
        {
            _chargeProviderDataService = chargeProviderDataService;
        }
        public async Task<IList<ChargeProvider>> GetChargingProviders()
        {
            return (await _chargeProviderDataService.GetAllAsync())
                .OrderBy(x=>x.ProviderName)
                .ToList();
        }
    }
}
