﻿using CarLog.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CarLog.Business.Services.DataListService
{
    public interface IDataListService
    {
        Task<IList<ChargeProvider>> GetChargingProviders();
    }
}
