﻿using CarLog.Domain.Requests;
using CarLog.Domain.Responses;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CarLog.Business.Services.ChargeService
{
    public interface IChargeService
    {
        Task<IList<ChargeResponse>> GetChargesAsync();
        Task<ChargeResponse> GetChargeAsync(int id);
        Task<ChargeResponse> CreateChargeAsync(ChargeRequest request);        
    }
}
