﻿using AutoMapper;
using CarLog.Domain.Entities;
using CarLog.Domain.Models;
using CarLog.Domain.Requests;
using CarLog.Domain.Responses;
using CarLog.EF.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CarLog.Business.Services.ChargeService
{
    public class ChargeService : IChargeService, IServiceMarker
    {
        private readonly IMapper _mapper;
        private readonly IDataService<ChargeLog> _dataService;

        public ChargeService(IDataService<ChargeLog> dataService, IMapper mapper)
        {
            _dataService = dataService;
            _mapper = mapper;
        }

        public async Task<ChargeResponse> CreateChargeAsync(ChargeRequest request)
        {
            ChargeResponse mappedResult = new ChargeResponse();
            ChargeLog result;
            var chargeLog = _mapper.Map<ChargeLog>(request);
            switch (request.StationAction)
            {
                case ActionType.ChargeStart:
                    result = await _dataService.CreateAsync(chargeLog);
                    mappedResult = _mapper.Map<ChargeResponse>(result);
                    break;
                case ActionType.ChargeEnd:
                    result = await _dataService.UpdateAsync(request.ChargeId, chargeLog);
                    mappedResult = _mapper.Map<ChargeResponse>(result);
                    break;                    
                default:
                    break;
            }            
            return mappedResult;
        }

        public async Task<ChargeResponse> GetChargeAsync(int id)
        {
            var result = await _dataService.GetAsync(id);
            var mappedResult = _mapper.Map<ChargeResponse>(result);
            return mappedResult;
        }

        public async Task<IList<ChargeResponse>> GetChargesAsync()
        {
            var result = await _dataService.GetAllAsync();
            var mappedResult = _mapper.Map<IList<ChargeResponse>>(result);
            return mappedResult;
        }
    }
}
