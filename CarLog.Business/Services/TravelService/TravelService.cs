﻿using AutoMapper;
using CarLog.Domain.Entities;
using CarLog.Domain.Requests;
using CarLog.Domain.Responses;
using CarLog.EF.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarLog.Business.Services.TravelService
{
    public class TravelService : ITravelService, IServiceMarker
    {
        private readonly IMapper _mapper;
        private readonly IDataService<TravelLog> _dataService;

        public TravelService(IMapper mapper, IDataService<TravelLog> dataService)
        {
            _mapper = mapper;
            _dataService = dataService;
        }

        public async Task<IList<TravelResponse>> GetTravelInterval(long startTicks, long endTicks)
        {
            var travelLog = await _dataService.GetFilteredData(x => x.Date >= new DateTime(startTicks) && x.Date <= new DateTime(endTicks));
            return _mapper.Map<IList<TravelResponse>>(travelLog);
        }

        public async Task<TravelResponse> GetTravelLogAsync(int id)
        {
            var travelLog = await _dataService.GetAsync(id);
            return _mapper.Map<TravelResponse>(travelLog);
        }

        public async Task<IList<TravelResponse>> GetTravelLogByDateAsync(long dateTicks)
        {
            var travelLog = await _dataService.GetFilteredData(x => x.Date.Date == new DateTime(dateTicks));            
            return _mapper.Map<IList<TravelResponse>>(travelLog);
        }

        public async Task<IList<TravelResponse>> GetTravelLogByMonthAsync(long dateTicks)
        {
            var timestamp = new DateTime(dateTicks);
            
            var year = timestamp.Year;
            var month = timestamp.Month;
            var firstDayOfMonth = new DateTime(year, month, 1);
            var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddSeconds(-1);
            
            var travelData = await _dataService.GetFilteredData(x => x.Date >= firstDayOfMonth && x.Date <= lastDayOfMonth);
            return _mapper.Map<IList<TravelResponse>>(travelData);

        }

        public async Task<IList<TravelResponse>> GetTravelLogsAsync()
        {
            var travelData = await _dataService.GetAllAsync();
            return _mapper.Map<IList<TravelResponse>>(travelData);
        }

        public async Task<TravelResponse> LogTravelAsync(TravelRequest request)
        {            
            var travelLog = _mapper.Map<TravelLog>(request);
            var travelLogResult = await _dataService.CreateAsync(travelLog);
            var result = _mapper.Map<TravelResponse>(travelLogResult);
            return result;
        }
    }
}
