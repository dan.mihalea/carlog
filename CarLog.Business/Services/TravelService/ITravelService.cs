﻿using CarLog.Domain.Requests;
using CarLog.Domain.Responses;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CarLog.Business.Services.TravelService
{
    public interface ITravelService
    {
        Task<IList<TravelResponse>> GetTravelLogsAsync();
        Task<TravelResponse> GetTravelLogAsync(int id);
        Task<TravelResponse> LogTravelAsync(TravelRequest request);
        Task<IList<TravelResponse>> GetTravelLogByDateAsync(long dateTicks);
        Task<IList<TravelResponse>> GetTravelLogByMonthAsync(long dateTicks);
        Task<IList<TravelResponse>> GetTravelInterval(long startTicks, long endTicks);
    }
}
