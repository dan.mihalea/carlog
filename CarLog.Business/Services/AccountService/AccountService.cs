﻿using AutoMapper;
using CarLog.Domain.Entities;
using CarLog.Domain.Models;
using CarLog.Domain.Requests;
using CarLog.EF;
using CarLog.EF.Services;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace CarLog.Business.Services.AccountService
{
    public class AccountService : IAccountService, IServiceMarker
    {
        private readonly IMapper _mapper;
        private readonly IDataService<User> _dataService;
        private readonly IDataService<Role> _roleDataService;

        public AccountService(IDataService<User> dataService, IMapper mapper, IDataService<Role> roleDataService)
        {
            _dataService = dataService;
            _mapper = mapper;
            _roleDataService = roleDataService;
        }


        //Authentication Methods
        public async Task<UserModel> LoginUserAsync(AuthenticationRequest request)
        {
            var user = (await _dataService
                .GetFilteredData(
                    r => r.UserName == request.UserName && r.Password == Encrypt(request.Password),
                    r => r.Roles,
                    r => r.UserCars
                 ))
                .FirstOrDefault();

            if (user == null)
                return null;

            var roles = await _roleDataService.GetFilteredData(x => user.Roles.Select(r => r.Role).Contains(x));


            return new UserModel()
            {
                Id = user.Id,
                UserName = user.UserName,
                Password = user.Password,
                FirstName = user.FirstName,
                LastName = user.LastName,
                Email = user.Email,
                IsActive = user.IsActive,
                Roles = user.Roles.Select(r=>r.Role.RoleName).ToArray(),
            };
        }
        public async Task<UserModel> GetUser(int id)
        {
            var user = (await _dataService
                .GetFilteredData(
                    r => r.Id == id,
                    r => r.Roles,
                    r => r.UserCars
                 ))
                .FirstOrDefault();

            if (user == null)
                return null;

            var roles = await _roleDataService.GetFilteredData(x => user.Roles.Select(r => r.Role).Contains(x));


            return new UserModel()
            {
                Id = user.Id,
                UserName = user.UserName,
                Password = user.Password,
                FirstName = user.FirstName,
                LastName = user.LastName,
                Email = user.Email,
                IsActive = user.IsActive,
                Roles = user.Roles.Select(r => r.Role.RoleName).ToArray(),
            };
        }
        public async Task<UserModel> RegisterUser(UserModel userModel)
        {
            var userAddressExists = (await _dataService
            .GetFilteredData(
                r => r.UserName == userModel.UserName && r.Password == Encrypt(userModel.Password),
                r => r.Roles,
                r => r.UserCars
             ))
            .FirstOrDefault();

            if (userAddressExists != null)
            {
                return null;
            }
            //var user = new User();

            //user.UserName = userModel.UserName;
            //user.Password = userModel.Password;
            //user.FirstName = userModel.FirstName;       
            //user.LastName = userModel.LastName; 
            //user.Email = userModel.Email;
            //user.IsActive = true;

            //var role = (await _roleDataService.GetFilteredData(r=>r.RoleName == "Contributor")).FirstOrDefault();

            //var userRole = new UserRole()
            //{
            //    User = user,
            //    Role = role
            //};


            return await GetUser(1);
        }

        private string Encrypt(string password)
        {
            var provider = MD5.Create();
            string salt = "El3ctr0Mobilitat3";
            byte[] bytes = provider.ComputeHash(Encoding.UTF32.GetBytes(salt + password));
            return BitConverter.ToString(bytes).Replace("-", "").ToLower();
        }

    }
}
