﻿using CarLog.Domain.Models;
using CarLog.Domain.Requests;
using System.Threading.Tasks;

namespace CarLog.Business.Services.AccountService
{
    public interface IAccountService
    {
        Task<UserModel> LoginUserAsync(AuthenticationRequest request);
        Task<UserModel> GetUser(int id);
        Task<UserModel> RegisterUser(UserModel userModel);
    }
}
