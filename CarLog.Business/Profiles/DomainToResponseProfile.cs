﻿using AutoMapper;
using CarLog.Domain.Entities;
using CarLog.Domain.Responses;
using System;
using System.Collections.Generic;
using System.Text;

namespace CarLog.Business.Profiles
{
    public class DomainToResponseProfile : Profile
    {
        public DomainToResponseProfile()
        {
            CreateMap<TravelLog, TravelResponse>()
                .ForMember(d => d.Id, opt => opt.MapFrom(s => s.Id))
                .ForMember(d => d.IsLegTravel, opt => opt.MapFrom(s => s.IsLeg))
                .ForMember(d => d.TravelMinutes, opt => opt.MapFrom(s => s.ElapsedMinutes))
                .ForMember(d => d.TravelDistance, opt => opt.MapFrom(s => s.Distance))
                .ForMember(d => d.TravelDate, opt => opt.MapFrom(s => s.Date))
                .ForMember(d => d.TravelTemperature, opt => opt.MapFrom(s => s.Temperature))
                .ForMember(d => d.TravelConsumption, opt => opt.MapFrom(s => s.Consumption))                
                .ForMember(d => d.TravelAverageSpeed,
                    opt => opt.MapFrom(s => s.ElapsedMinutes == 0
                    ? 0M
                    : Math.Round(s.Distance * 60 / s.ElapsedMinutes, 1)
                    ))
                .ForMember(d => d.TravelEnergyConsumed, opt => opt.MapFrom(s => Math.Round(s.Distance * s.Consumption / 100, 1)))
                ;

            CreateMap<ChargeLog, ChargeResponse>()
                .ForMember(d => d.ChargeId, opt => opt.MapFrom(s => s.Id))
                .ForMember(d => d.CarPercentageStart, opt => opt.MapFrom(s => s.StartProcent))
                .ForMember(d => d.CarPercentageEnd, opt => opt.MapFrom(s => s.EndProcent))
                .ForMember(d => d.ChargedMinutes, opt => opt.MapFrom(s => s.ChargingMinutes))
                .ForMember(d => d.ChargedUnits, opt => opt.MapFrom(s => s.ChargedUnits))
                .ForMember(d => d.IsPublicCharger, opt => opt.MapFrom(s => s.IsPublicCharger))
                .ForMember(d => d.StartIndex, opt => opt.MapFrom(s => s.StartIndex))
                .ForMember(d => d.EndIndex, opt => opt.MapFrom(s => s.EndIndex))
                .ForMember(d => d.IsActive, opt => opt.MapFrom(s => s.IsActive))
                .ForMember(d => d.StationProvider, opt => opt.MapFrom(s => s.ChargerProvider))
                .ForMember(d => d.StationPower, opt => opt.MapFrom(s => s.ChargerPower))
                .ForMember(d => d.UnitCost, opt => opt.MapFrom(s => s.UnitCost))
                .ForMember(d => d.ChargerType, opt => opt.MapFrom(s => s.SocketType))
                .ForMember(d => d.TimeStamp, opt => opt.MapFrom(s => s.TimeStamp))
                .ForMember(d=>d.StationProvider, opt=>opt.MapFrom(s=>s.ChargerProvider.Id))
                ;
        }
    }
}
