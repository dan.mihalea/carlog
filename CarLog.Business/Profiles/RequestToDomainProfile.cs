﻿using AutoMapper;
using CarLog.Domain.Entities;
using CarLog.Domain.Models;
using CarLog.Domain.Requests;

namespace CarLog.Business.Profiles
{
    public class RequestToDomainProfile : Profile
    {
        public RequestToDomainProfile()
        {
            CreateMap<TravelRequest, TravelLog>()
                .ForMember(s => s.Distance, opt => opt.MapFrom(d => d.TravelDistance))
                .ForMember(s=>s.IsLeg, opt=>opt.MapFrom(d=>d.IsLegTravel))
                .ForMember(s => s.Consumption, opt => opt.MapFrom(d => d.TravelConsumption))
                .ForMember(s => s.ElapsedMinutes, opt => opt.MapFrom(d => d.TravelMinutes))
                .ForMember(s => s.Date, opt => opt.MapFrom(d => d.TravelDate))
                .ForMember(s => s.Temperature, opt => opt.MapFrom(d => d.TravelTemperature))
                .ForMember(s => s.AverageSpeed, 
                    opt => opt.MapFrom(d => d.TravelMinutes == 0 
                    ? 0M
                    : d.TravelDistance * 60 / d.TravelMinutes))
                .ForMember(s => s.EnergyConsumed, opt => opt.MapFrom(d => d.TravelDistance * d.TravelConsumption / 100))
                ;

            CreateMap<ChargeRequest, ChargeLog>()
                .ForMember(d => d.ChargerPower, opt => opt.MapFrom(s => s.StationPower))
                .ForMember(d => d.ChargerProvider, opt => opt.MapFrom(s => s.StationProvider))
                .ForMember(d => d.ChargingMinutes, opt => opt.MapFrom(s => s.ChargedMinutes))
                .ForMember(d => d.Id, opt => opt.MapFrom(s => s.ChargeId))                
                //.ForMember(d => d.IsActive, opt => opt.MapFrom(s => s.ChargeId == 0))
                .ForMember(d => d.UnitCost, opt => opt.MapFrom(s => s.UnitCost))
                .ForMember(d => d.ChargingCost, opt => opt.MapFrom(s => s.UnitCost * s.ChargedUnits))
                .ForMember(d => d.StartProcent, opt => opt.MapFrom(s => s.CarPercentageStart))
                .ForMember(d => d.EndProcent, opt => opt.MapFrom(s => s.CarPercentageEnd))
                .ForMember(d=>d.SocketType, opt=>opt.MapFrom(s=>s.ChargerType))

                .ForMember(d => d.StartIndex, opt =>
                {
                    opt.Condition(s => s.StationAction == ActionType.ChargeStart);
                    opt.Condition(s => !s.IsPublicCharger);
                    opt.MapFrom(s => s.ChargeIndex);
                })
                .ForMember(d => d.EndIndex, opt =>
                  {
                      opt.Condition(s => s.StationAction == ActionType.ChargeEnd);
                      opt.Condition(s => !s.IsPublicCharger);
                      opt.MapFrom(s => s.ChargeIndex);
                  })
                .ForMember(d => d.ChargedUnits, opt =>
                {
                    opt.Condition(s => s.StationAction == ActionType.ChargeStart);
                    opt.MapFrom(s => 0);
                })
                .ForMember(d => d.ChargedUnits, opt =>
                {
                    opt.Condition(s => s.StationAction == ActionType.ChargeEnd);
                    opt.MapFrom(s => s.ChargedUnits);
                })
                ;
        }

    }
}
