﻿using CarLog.Domain.Requests;
using FluentValidation;

namespace CarLog.Domain.Validators
{
    public class LogTravelValidator : AbstractValidator<TravelRequest>
    {
        public LogTravelValidator()
        {
            RuleFor(x => x.TravelMinutes)
                .GreaterThan(0)
                ;
        }
    }
}
