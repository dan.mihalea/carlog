﻿using CarLog.Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace CarLog.EF
{
    public class CarLogContext : DbContext
    {
        public CarLogContext(DbContextOptions<CarLogContext> options)
            :base(options)
        {

        }

        DbSet<TravelLog> Travels { get; set; }
        DbSet<ChargeLog> Charges { get; set; }
        DbSet<User> Users { get; set; }
        DbSet<Role> Roles { get; set; }
        DbSet<CarModel> CarModels { get; set; }
        DbSet<CarManufacturer> CarManufacturers { get; set; }
        DbSet<ChargeProvider> ChargeProviders { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<UserRole>(u =>
            {
                u.HasKey(x => x.Id);
                u.Property(x => x.Id).ValueGeneratedOnAdd();
            });

            modelBuilder.Entity<UserRole>()
                .HasOne(ur => ur.User)
                .WithMany(ur => ur.Roles)
                .HasForeignKey(ur => ur.UserId);
            modelBuilder.Entity<UserRole>()
                .HasOne(ur => ur.Role)
                .WithMany(ur => ur.Users)
                .HasForeignKey(ur => ur.RoleId);

            modelBuilder.Entity<UserCar>(u =>
            {
                u.HasKey(x => x.Id);
                u.Property(x => x.Id).ValueGeneratedOnAdd();
            });

            modelBuilder.Entity<UserCar>()
                .HasOne(uc => uc.User)
                .WithMany(uc => uc.UserCars)
                .HasForeignKey(uc => uc.UserId);
            modelBuilder.Entity<UserCar>()
                .HasOne(m => m.CarManufacturer)
                .WithMany(m => m.UserCars)
                .HasForeignKey(m => m.CarManufacturerId);
            modelBuilder.Entity<UserCar>()
                .HasOne(m => m.CarModel)
                .WithMany(m => m.UserCars)
                .HasForeignKey(m => m.CarModelId);


            base.OnModelCreating(modelBuilder);
        }
    }
}
