﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;

namespace CarLog.EF
{
    public class ConfigurationManager : IConfigurationManager
    {
        public readonly IConfiguration configuration;

        public ConfigurationManager(IConfiguration configuration)
        {
            this.configuration = configuration;
        }

        public IConfigurationSection GetConfigurationSection(string sectionKey)
        {
            return configuration.GetSection(sectionKey);
        }

        public string GetConnectionString(string connectionName)
        {
            return configuration.GetConnectionString(connectionName);
        }
    }
}
