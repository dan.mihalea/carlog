﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CarLog.EF.Migrations
{
    public partial class InitialMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Charges",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    StartProcent = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    EndProcent = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    IsPublicCharger = table.Column<bool>(type: "bit", nullable: false),
                    ChargerPower = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    ChargerProvider = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    StartIndex = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    EndIndex = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    UnitCost = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    ChargingCost = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    ChargingMinutes = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Charges", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Travels",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Distance = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    Consumption = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    TravelMinutes = table.Column<int>(type: "int", nullable: false),
                    Temperature = table.Column<decimal>(type: "decimal(18,2)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Travels", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Charges");

            migrationBuilder.DropTable(
                name: "Travels");
        }
    }
}
