﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CarLog.EF.Migrations
{
    public partial class Add_ChargeProviders_SeedData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                insert into ChargeProviders (ProviderName)
                values
                ('Carge'),
                ('E.On'),
                ('eCharge'),
                ('Eldrive'),
                ('ELMotion'),
                ('Enel'),
                ('EnelX'),
                ('Engie'),
                ('EvConnect'),
                ('Fines Charging'),
                ('Home'),
                ('Hotel'),
                ('Ionity'),
                ('Kaufland'),
                ('Lidl'),
                ('Local Provider'),
                ('MCM Petrol'),
                ('Mobiliti'),
                ('Mobility+'),
                ('MOL Plugee'),
                ('Moon'),
                ('NextCharge'),
                ('Penny'),
                ('Plugpoint'),
                ('PlugSurfing'),
                ('Polyfazer'),
                ('Renovatio'),
                ('Rompetrol'),
                ('Smartrics'),
                ('Tesla Supercharger'),
                ('Lektri.co')
                ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
