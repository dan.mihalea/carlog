﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CarLog.EF.Migrations
{
    public partial class AddedChargedUnits : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<decimal>(
                name: "ChargedUnits",
                table: "Charges",
                type: "decimal(18,2)",
                nullable: false,
                defaultValue: 0m);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ChargedUnits",
                table: "Charges");
        }
    }
}
