﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CarLog.EF.Migrations
{
    public partial class Add_ChargeLog_ChargeProvider : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ChargerProvider",
                table: "Charges");

            migrationBuilder.AddColumn<int>(
                name: "ChargerProviderId",
                table: "Charges",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Charges_ChargerProviderId",
                table: "Charges",
                column: "ChargerProviderId");

            migrationBuilder.AddForeignKey(
                name: "FK_Charges_ChargeProviders_ChargerProviderId",
                table: "Charges",
                column: "ChargerProviderId",
                principalTable: "ChargeProviders",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);            
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Charges_ChargeProviders_ChargerProviderId",
                table: "Charges");

            migrationBuilder.DropIndex(
                name: "IX_Charges_ChargerProviderId",
                table: "Charges");

            migrationBuilder.DropColumn(
                name: "ChargerProviderId",
                table: "Charges");

            migrationBuilder.AddColumn<string>(
                name: "ChargerProvider",
                table: "Charges",
                type: "nvarchar(max)",
                nullable: true);
        }
    }
}
