﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace CarLog.EF.Migrations
{
    public partial class AddDateToTravel : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "TravelMinutes",
                table: "Travels",
                newName: "ElapsedMinutes");

            migrationBuilder.AddColumn<DateTime>(
                name: "Date",
                table: "Travels",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Date",
                table: "Travels");

            migrationBuilder.RenameColumn(
                name: "ElapsedMinutes",
                table: "Travels",
                newName: "TravelMinutes");
        }
    }
}
