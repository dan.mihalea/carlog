﻿using CarLog.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace CarLog.EF.Extensions
{
    public static class ContextExtensions
    {
        public static IQueryable<T> IncludeMany<T>(this IQueryable<T> query, params Expression<Func<T,object>>[] includes) where T : LogBase
        {
            if (includes != null)
            {
                query = includes.Aggregate(query,
                    (current, include) => current.Include(include));
            }
            return query;
        }
    }
}
