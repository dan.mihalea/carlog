﻿using CarLog.Domain.Entities;
using CarLog.EF.Extensions;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace CarLog.EF.Services
{
    public class GenericDataService<T> : IDataService<T> where T : LogBase
    {
        private readonly CarLogContext _context;

        public GenericDataService(
            CarLogContext context)
        {
            _context = context;
        }

        public async Task<T> CreateAsync(T entity)
        {
            EntityEntry<T> createdResult = await _context.Set<T>().AddAsync(entity);
            await _context.SaveChangesAsync();

            return createdResult.Entity;

        }

        public async Task<bool> DeleteAsync(int id)
        {

            T existingRecord = await _context.Set<T>().FirstOrDefaultAsync((e) => e.Id == id);
            _context.Set<T>().Remove(existingRecord);
            await _context.SaveChangesAsync();

            return true;

        }

        public async Task<T> GetAsync(int id)
        {

            T existingRecord = await _context.Set<T>().FirstOrDefaultAsync((e) => e.Id == id);
            return existingRecord;

        }

        public async Task<IEnumerable<T>> GetAllAsync()
        {

            IEnumerable<T> existingRecords = await _context.Set<T>().ToListAsync();
            return existingRecords;

        }

        public async Task<T> UpdateAsync(int id, T entity)
        {
            entity.Id = id;
            _context.Set<T>().Update(entity);
            await _context.SaveChangesAsync();

            return entity;

        }

        public async Task<IEnumerable<T>> GetFilteredData(Func<T, bool> predicate, params Expression<Func<T, object>>[] includedEntities)
        {

            IQueryable<T> existingRecords = _context.Set<T>();
            if (includedEntities != null)
            {
                existingRecords = existingRecords.IncludeMany(includedEntities);
            }
            var result = existingRecords
                .Where(predicate)
                .ToList();

            return await Task.FromResult(result);

        }
    }
}
