﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;

namespace CarLog.EF
{
    public interface IConfigurationManager
    {
        string GetConnectionString(string connectionName);
        IConfigurationSection GetConfigurationSection(string sectionKey);
    }
}
