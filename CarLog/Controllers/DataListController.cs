﻿using CarLog.Business.Services.DataListService;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarLog.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DataListController : ControllerBase
    {
        private readonly IDataListService _dataListService;
        public DataListController(IDataListService dataListService)
        {
            _dataListService = dataListService;
        }
        [HttpGet]
        [Route("providers")]
        public async Task<IActionResult> GetChargingProviders()
        {
            var result = await _dataListService.GetChargingProviders();
            return Ok(result);
        }
    }
}
