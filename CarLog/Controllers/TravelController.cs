﻿using CarLog.Business.Services.TravelService;
using CarLog.Domain.Requests;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace CarLog.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]

    public class TravelController : ControllerBase
    {
        private readonly ITravelService _travelService;
        public TravelController(ITravelService travelService)
        {
            this._travelService = travelService;
        }
        [HttpGet]
        public async Task<IActionResult> GetTravelLogs()
        {
            var result = await _travelService.GetTravelLogsAsync();
            return Ok(result);
        }
        [HttpGet]
        [Route("{id}")]
        [ActionName(nameof(GetTravelLog))]
        public async Task<IActionResult> GetTravelLog(int id)
        {
            var result = await _travelService.GetTravelLogAsync(id);
            return Ok(result);
        }
        [HttpGet]
        [Route("{dateTicks}/dayLog")]
        public async Task<IActionResult> GetTravelLogByDate(long dateTicks)
        {
            var result = await _travelService.GetTravelLogByDateAsync(dateTicks);
            return Ok(result);
        }
        [HttpGet]
        [Route("{dateTicks}/monthLog")]
        public async Task<IActionResult> GetTravelLogsByMonthAsync(long dateTicks)
        {
            var result = await _travelService.GetTravelLogByMonthAsync(dateTicks);
            return Ok(result);
        }
        [HttpGet]
        [Route("interval")]
        public async Task<IActionResult> GetTravelIntervalAsync([FromQuery]long startTicks, [FromQuery]long endTicks)
        {
            var result = await _travelService.GetTravelInterval(startTicks, endTicks);
            return Ok(result);
        }

        [HttpPost]
        public async Task<IActionResult> LogTravel([FromBody]TravelRequest request)
        {
            var result = await _travelService.LogTravelAsync(request);
            return CreatedAtAction(nameof(GetTravelLog),new { id = result.Id }, result);
        }
    }
}
