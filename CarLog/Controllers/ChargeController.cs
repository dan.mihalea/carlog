﻿using CarLog.Business.Services.ChargeService;
using CarLog.Domain.Requests;
using CarLog.Filters;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarLog.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class ChargeController : ControllerBase
    {

        private readonly IChargeService _chargeService;

        public ChargeController(IChargeService chargeService)
        {
            _chargeService = chargeService;
        }

        [HttpGet]
        public async Task<IActionResult> GetCharges()
        {
            var result = await _chargeService.GetChargesAsync();
            return Ok(result);
        }        

        [HttpGet]
        [Route("{id}")]
        [ActionName(nameof(GetCharge))]
        public async Task<IActionResult> GetCharge(int id)
        {
            var result = await _chargeService.GetChargeAsync(id);
            return Ok(result);
        }

        [HttpPost]
        public async Task<IActionResult> LogCharge([FromBody]ChargeRequest request)
        {            
            var result = await _chargeService.CreateChargeAsync(request);
            return CreatedAtAction(nameof(GetCharge), new { id = result.ChargedMinutes }, result);
        }        
    }
}
