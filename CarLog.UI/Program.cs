using Blazored.LocalStorage;
using CarLog.UI.Handlers;
using CarLog.UI.Services.ChargeService;
using CarLog.UI.Services.DataListService;
using CarLog.UI.Services.TravelService;
using CarLog.UI.ViewModels;
using CarLog.UI.ViewModels.Interfaces;
using Microsoft.AspNetCore.Components.Authorization;
using Microsoft.AspNetCore.Components.WebAssembly.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Syncfusion.Blazor;
using Syncfusion.Licensing;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace CarLog.UI
{
    public class Program
    {
        public static async Task Main(string[] args)
        {
            SyncfusionLicenseProvider.RegisterLicense("NTIwODI1QDMxMzkyZTMzMmUzMEMvTXM1dzNsNXMyT2pTMzd1Tko0VGErZ1k4aGJ5SFB4Rlh6ejR6c0c0N1U9;NTIwODI2QDMxMzkyZTMzMmUzMFg0Ymp3WlUyTVBSbElvMGtYY1lXTmFRMm5abkdoVGM5LzFXNmtjQkMyWDQ9;NTIwODI3QDMxMzkyZTMzMmUzMGo3aDJ4R1dZRUkyVE9KN1VJTzhRaU1WTFhVbmVjU0lzUG9wY3M5S1dIRm89;NTIwODI4QDMxMzkyZTMzMmUzME0zY0RlUmlWSTNkdHVoOUJYM3gvZ3h6ZWFFUFJscTNQb2N4cmRtdzc3ZFE9;NTIwODI5QDMxMzkyZTMzMmUzMFk1NkFLYTU3NWtXdUVzS2pPWXgvd2ZNbVpRa3g4cERhSVdRV2pNL0VzMG89;NTIwODMwQDMxMzkyZTMzMmUzME8rM1ZuTmhaNVFGZHJBK2pkamlrVm5xeVlUUmpRR0JjaVM4NHd0aGFUQlE9;NTIwODMxQDMxMzkyZTMzMmUzMGpKZEtsenE3djRBZjNYUmJKbUMrTGp4Rng1NUdtSkxWbjhibEhuTURaaEU9;NTIwODMyQDMxMzkyZTMzMmUzMEkxYXFnT2FqN2VFYy9FNDFXNUtWeVRvNE12c0EvNmdPWHJRUUpVK29OeEk9;NTIwODMzQDMxMzkyZTMzMmUzMFRDdlB3MEc3ZjNORnZFcWJIRHVaS2ZzcUtzSkZlT0haU1R5U0FzR3VmRms9;NTIwODM0QDMxMzkyZTMzMmUzME93THhJb2dyVXZNOGl4NHdTcnJEUVE1dGdwT25DWjZlU25Cajd0d1dMSG89;NTIwODM1QDMxMzkyZTMzMmUzMFl3bStTbmNlTC9WdzFBRXVJZTJUU2Z6K2l0YXU3cU52Yko5N1NQWjNzRTg9");

            var builder = WebAssemblyHostBuilder.CreateDefault(args);
                        
            builder.RootComponents.Add<App>("#app");
            var environment = builder.HostEnvironment;

            builder.Services.AddOptions();
            builder.Services.AddAuthorizationCore();

            AddHttpClients(builder, builder.Configuration);

            builder.Services.AddScoped<AuthenticationStateProvider, CustomAuthenticationStateProvider>();
            builder.Services.AddTransient<CustomAuthorizationHandler>();

            //var apiUrl = builder.Configuration.GetValue<string>("ApiUrl");
            //var baseUrl = builder.Configuration.GetValue<string>("BaseUrl");

            //builder.Services.AddHttpClient<ITravelService, TravelService>(client =>
            //{
            //    client.BaseAddress = new Uri(apiUrl);
            //    BaseUrl = baseUrl;
            //});

            //builder.Services.AddHttpClient<IChargeService, ChargeService>(client =>
            //{
            //    client.BaseAddress = new Uri(apiUrl);
            //    BaseUrl = baseUrl;
            //});

            builder.Services.AddSyncfusionBlazor();
            builder.Services.AddBlazoredLocalStorage();


            await builder.Build().RunAsync();
        }

        public static void AddHttpClients(WebAssemblyHostBuilder builder, IConfiguration configuration)
        {
            string baseAddress = configuration["BaseAddress"];

            builder.Services.AddScoped(sp => new HttpClient { BaseAddress = new Uri(baseAddress) });

            //transactional named http clients
            builder.Services.AddHttpClient<ITravelService, TravelService>
                ("TravelServiceClient", client => client.BaseAddress = new Uri(baseAddress))
                .AddHttpMessageHandler<CustomAuthorizationHandler>();
            builder.Services.AddHttpClient<IChargeService, ChargeService>
                ("ChargeService", client => client.BaseAddress = new Uri(baseAddress))
                .AddHttpMessageHandler<CustomAuthorizationHandler>();
            builder.Services.AddHttpClient<IDataListService, DataListService>
                ("DataListService", client => client.BaseAddress = new Uri(baseAddress))
                .AddHttpMessageHandler<CustomAuthorizationHandler>();
            builder.Services.AddHttpClient<IRegisterViewModel, RegisterViewModel>
                ("RegisterViewModelClient", client => client.BaseAddress = new Uri(baseAddress))
                .AddHttpMessageHandler<CustomAuthorizationHandler>();


            //authentication http clients
            builder.Services.AddHttpClient<ILoginViewModel, LoginViewModel>
                ("LoginViewModelClient", client => client.BaseAddress = new Uri(baseAddress));
        }
    }
}
