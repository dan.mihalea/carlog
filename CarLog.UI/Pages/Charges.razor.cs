﻿using CarLog.Domain.Entities;
using CarLog.Domain.Responses;
using CarLog.UI.Services.ChargeService;
using CarLog.UI.Services.DataListService;
using CarLog.UI.ViewModels;
using Microsoft.AspNetCore.Components;
using Syncfusion.Blazor.Buttons;
using Syncfusion.Blazor.DropDowns;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace CarLog.UI.Pages
{
    public partial class Charges
    {        
        [Inject]
        public IChargeService chargeService { get; set; }
        [Inject]
        public IDataListService dataList { get; set; }
        private ChargeViewModel _charge { get; set; } = new ChargeViewModel();
        public List<ChargeProvider> Providers { get; set; }
        public List<UserCar> Cars { get; set; }
        public bool IsLoading { get; set; }
        public bool ShowHomeIndex { get; set; }
        private Dictionary<string, object> addLogAttributes = new Dictionary<string, object>
        {
            { "title","Add charge log" },
            { "type","Submit"}
        };

        public SfCheckBox<bool> isPublicCharger;

        protected override async Task OnInitializedAsync()
        {
            IsLoading = true;
            Providers = (await dataList.GetChargeProviders())
                .OrderBy(x=>x.ProviderName)
                .ToList();
            IsLoading = false;
        }

        private void OnProviderChange(ChangeEventArgs<int?, ChargeProvider> args)
        {
            var homeProvider = Providers.FirstOrDefault(x => x.ProviderName == "Home");
            ShowHomeIndex = args.ItemData?.Id == homeProvider.Id;
        }
    }
}
