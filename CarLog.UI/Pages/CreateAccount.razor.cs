﻿using CarLog.UI.ViewModels.Interfaces;
using Microsoft.AspNetCore.Components;
using Syncfusion.Blazor.Notifications;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CarLog.UI.Pages
{
    public partial class CreateAccount
    {
        [Inject]
        public IRegisterViewModel _registerViewModel { get; set; }
        [Inject]
        public NavigationManager _navigationManager { get; set; }
        public Dictionary<string, object> registerHtmlAttributes = new Dictionary<string, object>
        {
            { "type","Submit"}
        };
        public string RegisterToastTitle { get; set; }
        public string RegisterToastContent { get; set; }
        public SfToast RegisterToast { get; set; }
        public string RegisterToastCss { get; set; }

        public async Task Register()
        {
            var registred = await _registerViewModel.Register();
            if (registred)
            {
                _navigationManager.NavigateTo("/", true);
            }
            else
            {
                RegisterToast.Content = "Username already exists!";
                RegisterToast.CssClass = "e-toast-danger";
                RegisterToast.ShowAsync();
            }
        }


    }
}
