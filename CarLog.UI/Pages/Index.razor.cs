﻿using CarLog.Domain.Responses;
using CarLog.UI.Extensions;
using CarLog.UI.Services.TravelService;
using CarLog.UI.ViewModels;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Web;
using Microsoft.Extensions.Logging;
using Syncfusion.Blazor.DropDowns;
using Syncfusion.Blazor.Grids;
using Syncfusion.Blazor.Navigations;
using Syncfusion.Blazor.Notifications;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace CarLog.UI.Pages
{
    public partial class Index
    {
        [Inject] 
        public ITravelService travelService { get; set; }
        [Inject]
        public ILoggerFactory loggerFactory  { get; set; }

        public ILogger logger { get; set; }
        public SfDropDownList<string, DropdownValue> WeekDdl { get; set; }
        public SfGrid<TravelResponse> TravelLogGrid { get; set; }
        public SfToast objToast { get; set; }
        public DropdownValue ddlValue;
        public IList<TravelResponse> Travels { get; set; }        
        public IList<TravelResponse> SelectedTravels { get; set; } = new List<TravelResponse>();
        public bool IsLoading { get; set; }        
        public List<DropdownValue> WeeksOfYear { get; set; }

        protected override async Task OnInitializedAsync()
        {
            logger = loggerFactory.CreateLogger<Index>();
            IsLoading = true;
            try
            {
                Travels = await travelService.GetAllTravels();
                WeeksOfYear = GetWeeksOfYear();
            }
            catch (Exception ex)
            {
                Travels = new List<TravelResponse>();
                objToast.Content = $"Something went wrong: {ex.Message}";
                await objToast.ShowAsync();
            }
            
            
            IsLoading = false;
        }
        public decimal GetTotalDistance()
        {
            return SelectedTravels.Sum(x => Convert.ToDecimal(x.TravelDistance));
        }
        public string GetTotalTripTime()
        {
            decimal totalMinutes = SelectedTravels.Sum(x => x.TravelMinutes);
            var hours = Math.Truncate(totalMinutes / 60);
            var minutes = totalMinutes - (hours * 60);
            return $"[{(int)totalMinutes} m] {(int)hours} h : {(int)minutes} m";
        }
        public decimal GetAverageConsumption()
        {
            if (GetTotalDistance() == 0) return 0;
            return Math.Round(SelectedTravels.Sum(x => x.TravelDistance * x.TravelConsumption) / GetTotalDistance(), 1);
        }
        public decimal GetAverageTemperature()
        {
            if (GetTotalDistance() == 0) return 0;
            return Math.Round(SelectedTravels.Sum(x => x.TravelDistance * x.TravelTemperature) / GetTotalDistance(), 1);
        }
        public decimal GetAverageSpeed()
        {
            if (GetTotalDistance() == 0) return 0;
            return Math.Round(SelectedTravels.Sum(x => x.TravelDistance * x.TravelAverageSpeed) / GetTotalDistance(), 1);
        }
        public decimal GetTotalEnergyConsumed()
        {
            return SelectedTravels.Sum(x => x.TravelEnergyConsumed);
        }
        public IList<TravelResponse> NormalizedData()
        {
            var result = new List<TravelResponse>();
            var multipleEntries = new List<TravelResponse>();

            foreach (var item in SelectedTravels)
            {
                if (multipleEntries.Count == 0)
                {
                    multipleEntries.Add(item);
                    continue;
                }
                if (multipleEntries.Any(x => x.TravelDate.ToString("yyyy.MM.dd") == item.TravelDate.ToString("yyyy.MM.dd")))
                {
                    multipleEntries.Add(item);
                    var computedData = multipleEntries.GetWeightedAverage();
                    multipleEntries.Clear();
                    multipleEntries.Add(computedData);
                    continue;
                }
                result.AddRange(multipleEntries);
                multipleEntries.Clear();
                multipleEntries.Add(item);
            }
            if (multipleEntries.Count > 0)
                result.AddRange(multipleEntries);
            return result;
        }

        private List<DateTime> GetDaysOfWeek(int weekNumber, DateTime time)
        {
            var today = time;
            int currentDayOfWeek = (int)today.DayOfWeek;
            DateTime sunday = today.Date.AddDays(-currentDayOfWeek);
            DateTime monday = sunday.AddDays(1);
            if (currentDayOfWeek == 0)
            {
                monday = monday.AddDays(-7);
            }
            var dates = Enumerable.Range(0, 7).Select(days => monday.AddDays(days)).ToList();
            return dates;
        }

        private List<DropdownValue> GetWeeksOfYear()
        {
            var result = new List<DropdownValue>();
            CultureInfo cultureInfo = new CultureInfo("ro-ro");
            Calendar calendar = cultureInfo.Calendar;

            DateTime lastDay = new DateTime(DateTime.Today.Year, 12, 31);
            var weekNumbers = calendar.GetWeekOfYear(lastDay, CalendarWeekRule.FirstDay, DayOfWeek.Monday);
            var firstWeek = calendar.GetWeekOfYear(Travels.First().TravelDate, CalendarWeekRule.FirstDay, DayOfWeek.Monday);
            int iterations = 0;
            for (int i = firstWeek; i <= weekNumbers; i++)
            {
                var daysRange = GetDaysOfWeek(i, Travels.First().TravelDate.AddDays(7 * iterations));
                result.Add(new DropdownValue
                {
                    Week = i,
                    StartDate = daysRange.Min(),
                    EndDate = daysRange.Max(),
                    WeekDays = $"Week {i}: {daysRange.Min().ToString("dd.MM.yyyy")} - {daysRange.Max().ToString("dd.MM.yyyy")}"
                });
                iterations++;
            }

            return result;
        }
        private void GetTravelsByInterval(DateTime startTime, DateTime endTime)
        {

        }

        private void ValueChangedHandler(ChangeEventArgs<string, DropdownValue> args)
        {
            var firstDayOfWeek = args.ItemData.StartDate.Date;
            var lastDayOfWeek = args.ItemData.EndDate.Date.AddDays(1).AddSeconds(-1);
            var travels = Travels
                .Where(x => x.TravelDate >= firstDayOfWeek && x.TravelDate <= lastDayOfWeek)
                .ToList();
            SelectedTravels = travels;

        }
        private async Task ToolbarClickHandler(ClickEventArgs args)
        {
            if (args.Item.Id == "TravelLogGrid_excelexport")
            {
                ExcelExportProperties exportProperties = new ExcelExportProperties
                {
                    IncludeHiddenColumn = true,
                    FileName = $"B64KWH_{WeekDdl.Value}.xlsx"
                };
                await TravelLogGrid.ExportToExcelAsync(exportProperties);
            }
        }
        private void onPreviousWeekClick(MouseEventArgs args)
        {
            GetTodayData();
            if (!string.IsNullOrWhiteSpace(WeekDdl.Value))
                WeekDdl.Index--;

        }
        private void onNextWeekClick(MouseEventArgs args)
        {
            GetTodayData();
            if (!string.IsNullOrWhiteSpace(WeekDdl.Value))
                WeekDdl.Index++;
        }        
        private void GetTodayData()
        {
            if (string.IsNullOrWhiteSpace(WeekDdl.Value))
            {
                var today = DateTime.Today;
                var index = WeeksOfYear
                    .FindIndex(x => today >= x.StartDate && today <= x.EndDate);
                WeekDdl.Index = index;
            }
        }
    }
}
