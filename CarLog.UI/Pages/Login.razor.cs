﻿
using Blazored.LocalStorage;
using CarLog.Domain.Responses;
using CarLog.UI.ViewModels.Interfaces;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Authorization;
using Syncfusion.Blazor.Notifications;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CarLog.UI.Pages
{
    public partial class Login
    {
        [CascadingParameter]
        public Task<AuthenticationState> authenticationState { get; set; }
        [Inject]
        public ILoginViewModel _loginViewModel { get; set; }
        [Inject]
        public ILocalStorageService _localStorageService { get; set; }
        [Inject]
        public NavigationManager _navigationManager { get; set; }
        public Dictionary<string, object> loginHtmlAttributes = new Dictionary<string, object>
        {            
            { "type","Submit"}
        };
        public string LoginToastTitle { get; set; }
        public string LoginToastContent { get; set; }
        public SfToast LoginToast { get; set; }
        public string LoginToastCss { get; set; }

        protected override async Task OnInitializedAsync()
        {
            //Initializing the login details for John Smith
//#if DEBUG
//            _loginViewModel.UserName = "etc";
//            _loginViewModel.Password = "etc";
//#endif
            var authState = await authenticationState;
            var user = authState.User;

            if (user.Identity.IsAuthenticated)
            {
                _navigationManager.NavigateTo("/");
            }
        }

        public async Task AuthenticateJWT()
        {
            AuthenticationResponse authenticationResponse = await _loginViewModel.AuthenticateJWT();
            if (authenticationResponse.Token != string.Empty)
            {
                await _localStorageService.SetItemAsync("jwt_token", authenticationResponse.Token);
                _navigationManager.NavigateTo("/", true);
            }
            else
            {
                LoginToast.Content = "Invalid username or password";
                LoginToast.CssClass = "e-toast-danger";
                LoginToast.ShowAsync();
            }
        }

    }

}
