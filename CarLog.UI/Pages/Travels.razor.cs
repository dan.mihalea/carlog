﻿using CarLog.Domain.Responses;
using CarLog.UI.Services.TravelService;
using CarLog.UI.ViewModels;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Forms;
using Syncfusion.Blazor.Buttons;
using Syncfusion.Blazor.Calendars;
using Syncfusion.Blazor.Notifications;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarLog.UI.Pages
{
    public partial class Travels
    {
        [Inject]
        public ITravelService travelService { get; set; }
        public string ToastContent => "Successfully added a new trip log";

        SfCheckBox<bool> isLegCheck;
        SfToast toastObj;
        private TravelViewModel _travel = new TravelViewModel();

        public bool IsLoading { get; set; }
        public IList<TravelResponse> TravelData { get; set; } = new List<TravelResponse>();
        protected override async Task OnInitializedAsync()
        {
            await GetData(_travel.TravelDate);
        }
        private Dictionary<string, object> addTripAttributes = new Dictionary<string, object>
        {
            { "title","Add trip" },
            { "type","Submit"}
        };
        private Dictionary<string, object> legTripAttributes = new Dictionary<string, object>
        {
            { "title","The leg trip denotes a different section of a longer trip" }
        };
        public async Task ValueChangeHandler(ChangedEventArgs<DateTime> args)
        {
            await GetData(args.Value);
        }
        private async Task SubmitTrip(EditContext context)
        {
            var model = (TravelViewModel)context.Model;


            var latestComplete = TravelData
                .Where(x => !x.IsLegTravel)
                .OrderByDescending(x => x.TravelDate)
                .FirstOrDefault();

            if (model.IsLeg)
            {
                var legTrips = TravelData
                    .Where(x => x.IsLegTravel && x.TravelDate >= latestComplete.TravelDate);

                var filteredTravels = new List<TravelResponse>(legTrips);
                filteredTravels.Add(latestComplete);

                var travelData = filteredTravels.OrderBy(x => x.TravelDate);

                var totalDistance = travelData.Sum(x => x.TravelDistance);
                var weithedConsumption = travelData.Sum(x => x.TravelDistance * x.TravelConsumption) / totalDistance;

                var legDistance = model.Distance - totalDistance;
                var legConsumption = ((model.Distance * model.Consumption) - (totalDistance * weithedConsumption)) / legDistance;
                var legElapsedMinutes = model.ElapsedMinutes - travelData.Sum(x => x.TravelMinutes);
                var temperature = model.Temperature;
                var date = model.TravelDate.Date != DateTime.Now.Date
                    ? travelData.OrderByDescending(x => x.TravelDate)?.FirstOrDefault()?.TravelDate.AddSeconds(1)
                    : model.TravelDate;

                model = new TravelViewModel
                {
                    Consumption = legConsumption,
                    Distance = legDistance,
                    ElapsedMinutes = legElapsedMinutes,
                    Temperature = temperature,
                    TravelDate = date.Value,
                    IsLeg = model.IsLeg
                };
            }
            else
            {
                var inputDate = model.TravelDate.Date;
                var resultedDate = DateTime.Now;
                if (inputDate != DateTime.Now.Date)
                {
                    resultedDate = latestComplete != null 
                        ? latestComplete.TravelDate 
                        : model.TravelDate.Date;
                }
                model.TravelDate = resultedDate.AddSeconds(1);
            }

            var result = await travelService.AddTravelAsync(model);
            if (result)
            {
                await toastObj.ShowAsync();
                _travel = new TravelViewModel();
            }
            else
            {
                toastObj.Content = "Something went wrong, and we couldn't add the trip log!";
                await toastObj.ShowAsync();
            }
            await GetData(DateTime.Now);
        }
        public async Task GetData(DateTime timestamp)
        {
            IsLoading = true;

            TravelData = await travelService.GetTravelsByDateAsync(timestamp.Date);
            _travel.IsLeg = TravelData.Count != 0;
            isLegCheck.Disabled = TravelData.Count == 0;

            IsLoading = !IsLoading;
        }
    }
}
