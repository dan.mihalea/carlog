﻿using Blazored.LocalStorage;
using Microsoft.AspNetCore.Components;
using System.Threading.Tasks;

namespace CarLog.UI.Pages
{
    public partial class Logout
    {

        [Inject]
        public ILocalStorageService _localStorageService { get; set; }
        [Inject]
        public NavigationManager _navigationManager { get; set; }

        protected override async Task OnInitializedAsync()
        {
            //await _httpClient.GetAsync("user/logoutuser");
            await _localStorageService.RemoveItemAsync("jwt_token");
            _navigationManager.NavigateTo("/", true);
        }
    }
}
