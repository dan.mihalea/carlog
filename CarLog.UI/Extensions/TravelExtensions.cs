﻿using CarLog.Domain.Responses;
using CarLog.UI.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarLog.UI.Extensions
{
    public static class TravelExtensions
    {
        public static TravelResponse GetWeightedAverage(this IList<TravelResponse> values)
        {
            var result = new TravelResponse();
            if (values.Count == 0)
                return result;

            result.TravelDate = values.First().TravelDate;
            result.TravelDistance = values.Sum(x => x.TravelDistance);
            result.TravelConsumption = Math.Round(values.Sum(x => x.TravelDistance * x.TravelConsumption) / result.TravelDistance, 1);
            result.TravelAverageSpeed = Math.Round(values.Sum(x => x.TravelDistance * x.TravelAverageSpeed) / result.TravelDistance, 1);
            result.TravelTemperature = Math.Round(values.Sum(x => x.TravelDistance * x.TravelTemperature) / result.TravelDistance, 1);
            result.TravelEnergyConsumed = values.Sum(x => x.TravelEnergyConsumed);
            return result;
        }

        public static DropdownValue GetWeek(this DateTime timestamp)
        {
            var today = timestamp;
            int currentDayOfWeek = (int)today.DayOfWeek;
            DateTime sunday = today.AddDays(-currentDayOfWeek);
            DateTime monday = sunday.AddDays(1);
            if (currentDayOfWeek == 0)
            {
                monday = monday.AddDays(-7);
            }
            var dates = Enumerable.Range(0, 7)
                .Select(days => monday.AddDays(days))
                .ToList();
            return new DropdownValue
            {
                StartDate = dates.Min(),
                EndDate = dates.Max(),
                WeekDays = $"{dates.Min().ToString("dd.MM.yyyy")} - {dates.Max().ToString("dd.MM.yyyy")}"
            };
        }
    }
}
