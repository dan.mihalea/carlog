﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarLog.UI.Models
{
    public enum ReportType
    {        
        Week,
        Month,
        Year
    }
}
