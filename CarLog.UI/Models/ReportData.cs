﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarLog.UI.Models
{
    public class ReportData
    {
        public string ReportTitle { get; set; }
        public string ReportName { get; set; }
        public string PreviousButtonContent { get; set; }
        public string NextButtonContent { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }
}
