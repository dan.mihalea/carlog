﻿using CarLog.Domain.Responses;
using CarLog.UI.Business.Report;
using CarLog.UI.Extensions;
using CarLog.UI.Models;
using CarLog.UI.Services.TravelService;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarLog.UI.Components
{
    public partial class TimeReport
    {
        [Inject]
        public ITravelService travelService { get; set; }
        [Parameter]
        public ReportType ReportType { get; set; }

        public IList<TravelResponse> DataSource { get; set; }
        public TravelResponse Result { get; set; }        
        public string ReportName { get; set; }
        public string ReportTitle { get; set; }
        private string prevButtonContent { get; set; }
        private string nextButtonContent { get; set; }
        private DateTime _currentTimeStamp { get; set; } = DateTime.Now;
        private IReport report;
        private ReportData reportResult;

        protected override async Task OnInitializedAsync()
        {            
            await UpdateDataAsync(DateTime.Now);
        }

        private async Task UpdateDataAsync(DateTime timestamp)
        {
            report = new ReportStrategy(ReportType, _currentTimeStamp).GetReport(ReportType);
            reportResult = report.GetReportData();

            ReportName = reportResult.ReportName;
            ReportTitle = reportResult.ReportTitle;
            prevButtonContent = reportResult.PreviousButtonContent;
            nextButtonContent = reportResult.NextButtonContent;

            DataSource = await travelService.GetTravelByIntervalAsync(reportResult.StartDate, reportResult.EndDate);

            Result = DataSource.GetWeightedAverage();
        }
        private async Task onPreviousClick(MouseEventArgs args)
        {
            var getDateTime = report.OnPrevious();            
            _currentTimeStamp = getDateTime;
            await UpdateDataAsync(getDateTime);
            StateHasChanged();
        }
        private async Task onNextClick(MouseEventArgs args)
        {
            var getDateTime = report.OnNext();
            _currentTimeStamp = getDateTime;
            await UpdateDataAsync(getDateTime);
            StateHasChanged();
        }
    }
}
