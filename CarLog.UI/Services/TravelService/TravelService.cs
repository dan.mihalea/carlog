﻿using CarLog.Domain.Requests;
using CarLog.Domain.Responses;
using CarLog.UI.ViewModels;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;

namespace CarLog.UI.Services.TravelService
{
    public class TravelService : ITravelService
    {
        private readonly HttpClient _client;

        public TravelService(HttpClient client)
        {
            _client = client;
        }

        public async Task<IList<TravelResponse>> GetAllTravels()
        {
            return await _client.GetFromJsonAsync<IList<TravelResponse>>($"travel");
        }
        public async Task<bool> AddTravelAsync(TravelViewModel data)
        {            
            var travelRequest = new TravelRequest
            {
                TravelConsumption = data.Consumption.Value,
                TravelDate = data.TravelDate,
                TravelDistance = data.Distance.Value,
                TravelMinutes = data.ElapsedMinutes.Value,
                TravelTemperature = data.Temperature.Value,
                IsLegTravel = data.IsLeg
            };

            var result = await _client.PostAsJsonAsync($"travel", travelRequest);
            if (result.StatusCode == HttpStatusCode.Created)
            {
                return true;
            }
            return false;
        }
        public async Task<IList<TravelResponse>> GetTravelsByDateAsync(DateTime timestamp)
        {
            var ticks = timestamp.Date.Ticks;
            return await _client.GetFromJsonAsync<IList<TravelResponse>>($"travel/{ticks}/dayLog");
        }
        public Task<TravelResponse> GetTravel(int id)
        {
            throw new NotImplementedException();
        }
        public async Task<IList<TravelResponse>> GetTravelByIntervalAsync(DateTime startTimeStamp, DateTime endTimeStamp)
        {
            var start = startTimeStamp.Ticks;
            var end = endTimeStamp.Ticks;
            return await _client.GetFromJsonAsync<IList<TravelResponse>>($"travel/interval?startTicks={start}&endTicks={end}");
        }
    }
}
