﻿using CarLog.Domain.Responses;
using CarLog.UI.ViewModels;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CarLog.UI.Services.TravelService
{
    public interface ITravelService
    {
        Task<IList<TravelResponse>> GetAllTravels();
        Task<TravelResponse> GetTravel(int id);
        Task<IList<TravelResponse>> GetTravelsByDateAsync(DateTime timestamp);
        Task<IList<TravelResponse>> GetTravelByIntervalAsync(DateTime startTimeStamp, DateTime endTimeStamp);
        Task<bool> AddTravelAsync(TravelViewModel data);
    }
}
