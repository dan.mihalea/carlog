﻿using CarLog.Domain.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarLog.UI.Services.ChargeService
{
    public interface IChargeService
    {
        Task<IList<ChargeResponse>> GetCharges();
    }
}
