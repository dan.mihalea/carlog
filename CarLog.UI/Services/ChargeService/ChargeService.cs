﻿using CarLog.Domain.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;

namespace CarLog.UI.Services.ChargeService
{
    public class ChargeService : IChargeService
    {
        private readonly HttpClient client;

        public ChargeService(HttpClient client)
        {
            this.client = client;
        }

        public async Task<IList<ChargeResponse>> GetCharges()
        {
            return await client.GetFromJsonAsync<IList<ChargeResponse>>($"charge");
        }
    }
}
