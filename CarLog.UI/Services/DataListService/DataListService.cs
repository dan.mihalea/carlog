﻿using CarLog.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;

namespace CarLog.UI.Services.DataListService
{
    public class DataListService : IDataListService
    {
        public readonly HttpClient _client;

        public DataListService(HttpClient client)
        {
            _client = client;
        }

        public Task<IList<ChargeProvider>> GetChargeProviders()
        {
            return _client.GetFromJsonAsync<IList<ChargeProvider>>($"datalist/providers");
        }
    }
}
