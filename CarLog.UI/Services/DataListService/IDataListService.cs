﻿using CarLog.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarLog.UI.Services.DataListService
{
    public interface IDataListService
    {
        Task<IList<ChargeProvider>> GetChargeProviders();
    }
}
