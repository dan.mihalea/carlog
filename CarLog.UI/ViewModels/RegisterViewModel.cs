﻿using CarLog.Domain.Models;
using CarLog.UI.ViewModels.Interfaces;
using System;
using System.ComponentModel.DataAnnotations;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;

namespace CarLog.UI.ViewModels
{
    public class RegisterViewModel : IRegisterViewModel
    {
        [Required]
        public string UserName { get; set; }
        [Required]
        public string Password { get; set; }
        [Required]
        public string Email { get; set; }
        [Required]
        public string LastName { get; set; }
        [Required]
        public string FirstName { get; set; }

        private HttpClient _httpClient;
        public RegisterViewModel()
        {

        }
        public RegisterViewModel(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public async Task<bool> Register()
        {
            Console.WriteLine(this.UserName);
            Console.WriteLine(this.Password);
            Console.WriteLine(this.Email);
            Console.WriteLine(this.LastName);
            Console.WriteLine(this.FirstName);
            var httpMessageReponse = await _httpClient.PostAsJsonAsync<UserModel>("accounts/registeruser", this);
            var user = await httpMessageReponse.Content.ReadFromJsonAsync<UserModel>();

            return user != null;
        }

        public static implicit operator RegisterViewModel(UserModel user)
        {
            return new RegisterViewModel
            {
                UserName = user.UserName,
                Email = user.Email,
                Password = user.Password,
                LastName = user.LastName,
                FirstName = user.FirstName,

            };
        }

        public static implicit operator UserModel(RegisterViewModel registerViewModel)
        {
            return new UserModel
            {
                UserName = registerViewModel.UserName,
                Password = registerViewModel.Password,
                Email = registerViewModel.Email,
                LastName = registerViewModel.LastName,
                FirstName = registerViewModel.FirstName,
            };
        }
    }

}
