﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarLog.UI.ViewModels
{
    public class DropdownValue
    {
        public int Week { get; set; }
        public string WeekDays { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }
}
