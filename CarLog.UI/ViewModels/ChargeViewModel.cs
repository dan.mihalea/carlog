﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CarLog.UI.ViewModels
{
    public class ChargeViewModel
    {
        [Required]
        [Range(0,100, ErrorMessage = "Accepted range is between { 1 } and { 2 }")]
        public decimal StartPercent { get; set; }
        [Required]
        public decimal EndPercent { get; set; }
        public decimal ChargedUnits { get; set; }
        public decimal UnitCost { get; set; }
        [Required]
        [Range(0,3600, ErrorMessage = "Accepted range is between { 1 } and { 2 }")]
        public int ChargedMinutes { get; set; }
        public bool IsPublicCharger { get; set; }
        public decimal StartIndex { get; set; }
        public decimal EndIndex { get; set; }
        [Required]
        public int StationProviderId { get; set; }
        public decimal StationPower { get; set; }
        public DateTime TimeStamp { get; set; } = DateTime.Now;
        public int UserCarId { get; set; }
        public bool IsComputedCharge { get; set; }
    }
}
