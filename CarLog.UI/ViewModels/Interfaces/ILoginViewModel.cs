﻿using CarLog.Domain.Models;
using CarLog.Domain.Responses;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CarLog.UI.ViewModels.Interfaces
{
    public interface ILoginViewModel
    {        
        public string UserName { get; set; }        
        public string Password { get; set; }
        public bool RememberMe { get; set; }
        public Task LoginUser();
        public Task<AuthenticationResponse> AuthenticateJWT();
        public Task<UserModel> GetUserByJWTAsync(string jwtToken);
    }
}
