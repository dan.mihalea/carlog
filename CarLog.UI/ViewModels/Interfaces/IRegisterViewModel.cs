﻿using System.Threading.Tasks;

namespace CarLog.UI.ViewModels.Interfaces
{
    public interface IRegisterViewModel
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }



        public Task<bool> Register();
    }
}
