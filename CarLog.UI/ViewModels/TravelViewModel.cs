﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CarLog.UI.ViewModels
{
    public class TravelViewModel
    {
        [Required]
        [Range(0, int.MaxValue, ErrorMessage = "Value must be greater than {1}")]
        public decimal? Distance { get; set; }
        [Required]
        [Range(-100, 100, ErrorMessage = "Accepted range is between {1} and {2}")]
        public decimal? Consumption { get; set; }
        [Required]
        [Range(0, int.MaxValue, ErrorMessage = "Value must be greater than {1}")]
        public int? ElapsedMinutes { get; set; }
        [Range(-50, 75, ErrorMessage = "Accepted range is between {1} and {2}")]
        public decimal? Temperature { get; set; }
        public bool IsLeg { get; set; }
        [Required]
        public DateTime TravelDate { get; set; } = DateTime.Now;
    }
}
