﻿using CarLog.Domain.Models;
using CarLog.Domain.Requests;
using CarLog.Domain.Responses;
using CarLog.UI.ViewModels.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;

namespace CarLog.UI.ViewModels
{
    public class LoginViewModel : ILoginViewModel
    {
        public string UserName { get; set; }        
        public string Password { get; set; }
        public bool RememberMe { get; set; }

        private HttpClient _httpClient;
        public LoginViewModel()
        {

        }
        public LoginViewModel(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public async Task LoginUser()
        {
            await _httpClient.PostAsJsonAsync<UserModel>($"user/loginuser?isPersistent={this.RememberMe}", this);
        }

        public async Task<AuthenticationResponse> AuthenticateJWT()
        {
            //creating authentication request
            AuthenticationRequest authenticationRequest = new AuthenticationRequest();
            authenticationRequest.UserName = this.UserName;
            authenticationRequest.Password = this.Password;

            var s = _httpClient.BaseAddress;
            //authenticating the request
            var httpMessageReponse = await _httpClient.PostAsJsonAsync<AuthenticationRequest>($"accounts/authenticatejwt", authenticationRequest);

            //sending the token to the client to store
            return await httpMessageReponse.Content.ReadFromJsonAsync<AuthenticationResponse>();
        }

        public async Task<UserModel> GetUserByJWTAsync(string jwtToken)
        {
            //preparing the http request
            var requestMessage = new HttpRequestMessage(HttpMethod.Post, "accounts/getuserbyjwt");
            requestMessage.Content = new StringContent(jwtToken);

            requestMessage.Content.Headers.ContentType
                = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");

            //making the http request
            var response = await _httpClient.SendAsync(requestMessage);

            var responseStatusCode = response.StatusCode;
            var returnedUser = await response.Content.ReadFromJsonAsync<UserModel>();

            //returning the user if found
            if (returnedUser != null) return await Task.FromResult(returnedUser);
            else return null;
        }

        public static implicit operator LoginViewModel(UserModel user)
        {
            return new LoginViewModel
            {
                UserName = user.UserName,
                Password = user.Password
            };
        }

        public static implicit operator UserModel(LoginViewModel loginViewModel)
        {
            return new UserModel
            {
                UserName = loginViewModel.UserName,
                Password = loginViewModel.Password
            };
        }
    }

}
