﻿using CarLog.UI.Models;
using CarLog.UI.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarLog.UI.Business.Report
{
    public interface IReport
    {
        ReportData GetReportData();
        DateTime OnPrevious();
        DateTime OnNext();
    }
}
