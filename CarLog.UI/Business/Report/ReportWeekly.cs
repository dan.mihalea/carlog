﻿using CarLog.UI.Extensions;
using CarLog.UI.Models;
using System;

namespace CarLog.UI.Business.Report
{
    public class ReportWeekly : IReport
    {
        private readonly DateTime _timeStamp;
        public ReportWeekly(DateTime timeStamp)
        {
            _timeStamp = timeStamp;
        }
        public ReportData GetReportData()
        {
            ReportData result = new ReportData();
            var weekSpan = _timeStamp.GetWeek();

            result.PreviousButtonContent = $"< Previous week";
            result.NextButtonContent = $"Next week >";

            result.StartDate = weekSpan.StartDate.Date;
            result.EndDate = weekSpan.EndDate.Date.AddDays(1).AddSeconds(-1);

            result.ReportTitle = "Weekly report";
            result.ReportName = $"{weekSpan.StartDate.ToString("dd.MM.yyyy")} - {weekSpan.EndDate.ToString("dd.MM.yyyy")}";

            return result;
        }

        public DateTime OnNext()
        {
            return _timeStamp.AddDays(7);
        }

        public DateTime OnPrevious()
        {
            return _timeStamp.AddDays(-7);
        }
    }
}
