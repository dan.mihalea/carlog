﻿using CarLog.UI.Models;
using CarLog.UI.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarLog.UI.Business.Report
{
    public class ReportMonthly : IReport
    {
        private readonly DateTime _timeStamp;
        public ReportMonthly(DateTime timeStamp)
        {
            _timeStamp = timeStamp;
        }        

        public ReportData GetReportData()
        {
            ReportData result = new ReportData();
            result.PreviousButtonContent = $"< {_timeStamp.AddMonths(-1).ToString("MMMM")}";
            result.NextButtonContent = $"{_timeStamp.AddMonths(1).ToString("MMMM")} >";

            result.StartDate = new DateTime(_timeStamp.Year, _timeStamp.Month, 1);
            result.EndDate = result.StartDate.Date.AddMonths(1).AddSeconds(-1);

            result.ReportTitle = "Monthly report";
            result.ReportName = $"{_timeStamp.ToString("MMMM")} {_timeStamp.Year}";

            return result;
        }

        public DateTime OnNext()
        {
            return _timeStamp.AddMonths(1);
        }

        public DateTime OnPrevious()
        {
            return _timeStamp.AddMonths(-1);
        }
    }
}
