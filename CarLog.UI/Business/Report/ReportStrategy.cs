﻿using CarLog.UI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarLog.UI.Business.Report
{
    public class ReportStrategy
    {
        private ReportType _reportType;
        private DateTime _timeStamp;
        public ReportStrategy(ReportType reportType, DateTime timeStamp)
        {
            this._reportType = reportType;
            _timeStamp = timeStamp;
        }
        public IReport GetReport(ReportType reportType)
        {
            switch (reportType)
            {
                case ReportType.Week:
                    return new ReportWeekly(_timeStamp);
                case ReportType.Month:
                    return new ReportMonthly(_timeStamp);
                case ReportType.Year:                    
                default:
                    return null;                    
            }
        }
    }
}
